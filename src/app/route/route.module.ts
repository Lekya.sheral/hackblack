import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MartComponent } from '../mart/mart.component';
import { RetailerComponent } from '../retailer/retailer.component';
import { BoardComponent } from '../board/board.component';
import { DistilleryComponent } from '../distillery/distillery.component';
import { LoginComponent } from '../login/login.component';
const routes: Routes = [
 {
   path: 'mart',
   component: MartComponent,
   pathMatch: 'full'
 },
 {
   path: 'retailer',
   component: RetailerComponent,
   pathMatch: 'full'
 },
 {
   path: 'board',
   component: BoardComponent,
   pathMatch: 'full'
 },
 {
   path: 'distillery',
   component: DistilleryComponent,
   pathMatch: 'full'
 },
 {
   path: 'login',
   component: LoginComponent,
   pathMatch: 'full'
 },
 {
   path: '',
   component: LoginComponent
 }

]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule],
  declarations: []
})
export class RouteModule { }
