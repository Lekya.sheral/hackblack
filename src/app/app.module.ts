import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AuthService } from './services/auth.service';
import { ContractService } from './services/contract.service';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MartComponent } from './mart/mart.component';
import { RetailerComponent } from './retailer/retailer.component';
import { BoardComponent } from './board/board.component';
import { DistilleryComponent } from './distillery/distillery.component';
import { LoginComponent } from './login/login.component';

import { IpfsService } from './services/ipfs.service'
import { RouteModule } from './route/route.module'

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    RouteModule
  ],
  declarations: [
    AppComponent,
    MartComponent,
    RetailerComponent,
    BoardComponent,
    DistilleryComponent,
    LoginComponent
  ],
  providers: [AuthService, ContractService, IpfsService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
  }
}
